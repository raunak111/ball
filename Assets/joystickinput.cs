﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class joystickinput : MonoBehaviour
{
    public float speed;
    public FloatingJoystick myJs;
    public void AdjustSpeed(float newSpeed) { speed = newSpeed; }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 direction = Vector3.forward * myJs.Vertical + Vector3.right * myJs.Horizontal;
        GetComponent<Rigidbody>().AddForce(direction*speed);
    }
}
