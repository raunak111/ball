﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Diagnostics;

public class destroypanel : MonoBehaviour
{
    bool isDone = false;

    public GameObject panel1;
    public GameObject panel2;
    public GameObject panel3;
    // Start is called before the first frame update
    void Start()
    {
        //PlayerPrefs.SetInt("hasintrodisplayed", 0);

        if (PlayerPrefs.GetInt("hasintrodisplayed") == 0)
        {
            panel1.SetActive(true);
            panel2.SetActive(true);
            panel3.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDone)
        {
            if (Input.touchCount > 0)
            {
                UnityEngine.Debug.Log("tap recieved");
                Destroy(panel1);
                Destroy(panel2);
                Destroy(panel3);
                PlayerPrefs.SetInt("hasintrodisplayed", 1);
                isDone = true;
            }
        }
        

        /*if (Input.GetMouseButtonDown(0)) {
            UnityEngine.Debug.Log("mouse clicked");
            Destroy(panel);
        }*/
    }
}
