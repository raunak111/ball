﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class updateslidervalue : MonoBehaviour
{
    public Text speedval;
    public Slider speedslider;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        speedval.text = speedslider.value.ToString();
    }
}
