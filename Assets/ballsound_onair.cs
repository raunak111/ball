﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballsound_onair : MonoBehaviour
{
    public AudioSource tickSource;
    /*void OnCollisionEnter() {
        tickSource.mute;
    }*/

    void OnCollisionExit() {
        tickSource.Play();
    }

    void OnCollisionStay() {
        tickSource.Stop();
    }

    // Start is called before the first frame update
    void Start()
    {
        tickSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
