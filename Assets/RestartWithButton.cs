﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class RestartWithButton : MonoBehaviour
{
    public GameObject player;
    public void restart() {
        player.transform.position = GameManager.Instance.lastCheckPoint.position;
        player.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
    
    //public void OnPointerClick(PointerEventData eventData) { SceneManager.LoadScene(SceneManager.GetActiveScene().name); }
    
    //public Texture btnTexture;

    /*void OnGUI()
    {
        if (!btnTexture)
        {
            Debug.LogError("Please assign a texture on the inspector");
            return;
        }

        if (GUI.Button(new Rect(675, 10, 50, 50), btnTexture))
        { SceneManager.LoadScene(SceneManager.GetActiveScene().name); }
        //Debug.Log("Clicked the button with an image");

    }*/

    // Update is called once per frame
   /* void Update()
    {
        if (Input.GetKeyDown(myButton)) { SceneManager.LoadScene(SceneManager.GetActiveScene().name); }
    }*/
}
