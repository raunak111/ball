﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class activatesettingpanel : MonoBehaviour
{
    public GameObject myPlayer;
    public GameObject myPanel;
    //public GameObject joystick;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void activate()
    {
        Debug.Log("Pressed Paused");
        if (myPanel.activeInHierarchy) {
            myPanel.SetActive(false);
            myPlayer.SetActive(true);
            //myPlayer.GetComponent<touch>().enabled = true;
        }
        else {
            myPanel.SetActive(true);
            myPlayer.SetActive(false);
            //myPlayer.GetComponent<touch>().enabled = false;
        }
    }
}
