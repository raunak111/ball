﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class RestartOnCollision : MonoBehaviour
{
    /*private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Player")
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }*/

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            other.gameObject.transform.position = GameManager.Instance.lastCheckPoint.position;
            other.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }
}
